import java.util.Scanner;
import static java.lang.System.*;

public class Ex38{
    public static void main(String[] args){
        Scanner keyboard = new Scanner(in);

        int choice;
        double area = 0;

        out.println("Shape area calculator\n");

        do {
            out.println("\nChoose between these choices");
            out.println("\n 1. Triangle");
            out.println("\n 2. Circle");
            out.println("\n 3. Rectangle");
            out.println("\n 4. Quit");

            out.print("\n> ");
            choice = keyboard.nextInt();

            if(choice==1){
                out.print("\nBase: ");
                int b = keyboard.nextInt();//Declare and initialize var on same line
                out.print("\nHeight: ");
                int h = keyboard.nextInt();
                area = computeTriangleArea(b,h);
                out.print("\nThe area is "+area);
            }
            else if(choice==2){
                out.print("\nBase: ");
                int r = keyboard.nextInt();
                area = computeCircleArea(r);
                out.println("\nThe area is "+area);
            }
            else if(choice==3){
                out.print("\nLength: ");
                int length = keyboard.nextInt();
                out.print("\nWidth: ");
                int width = keyboard.nextInt();
                area = computeRectangleArea(length,width);
                out.print("\n The area is "+area);
            }
            else if(choice != 4){
                out.println("error");
            }
        } while (choice !=4);
        // When the choice is 4, the main() function has no choice but to end the program
    }

    public static double computeTriangleArea(int base, int height){
        double A;
        A = 0.5*base*height;
        return A;
    }

    public static double computeCircleArea(int radius){
        double A;
        A = Math.PI * radius* radius;
        return A;
    }

    public static double computeRectangleArea(int length, int width){
        return (length*width);
    }
}
