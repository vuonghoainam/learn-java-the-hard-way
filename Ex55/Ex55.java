// Put record/ class into array
import static java.lang.System.*;

class Student{
    String name;
    int credits;
    double gpa;
}

public class Ex55{
    public static void main(String[] args){
        Student[] db = new Student[3];// This line is to be noticed

        db[0] = new Student();
        db[0].name = "Esteban";
        db[0].credits = 43;
        db[0].gpa = 4.0;

        db[1] = new Student();
        db[1].name = "Esteban";
        db[1].credits = 43;
        db[1].gpa = 4.0;

        db[2] = new Student();
        db[2].name = "Esteban";
        db[2].credits = 43;
        db[2].gpa = 4.0;

        for(int i=0;i<db.length;i++){
            out.println("Name:" +db[i].name);
        }
    }
}
