// Saving a high score

import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import static java.lang.System.*;

public class Ex43
{
    public static void main(String[] args) throws Exception
    {
        Scanner kb = new Scanner(in);

        String coin, again, bestName, saveFile ="letter.txt";
        int flip, streak = 0, best;

        File inputFile = new File(saveFile);

        if(inputFile.createNewFile())
        {
            out.println("Save game file does not exist. Created!");
            best = -1;
            bestName = "";
        }
        else
        {
            Scanner inputKB = new Scanner(inputFile);
            bestName = inputKB.nextLine();
            best = inputKB.nextInt();
            inputKB.close();
            out.println("High score is "+best+" flips in a row by "+bestName);
        }

        do
        {
            flip = 1 + (int)(Math.random()*2);

            if(flip==1)
            {
                coin = "Heads";
            }
            else
            {
                coin = "Tails";
            }

            out.println("You flip a coin and it is "+coin);

            if(flip==1)
            {
                streak++;
                out.println("\n That's "+ streak+" in a row...");
                out.print("\nWould you like to flip again(y/n)? ");
                again = kb.next();
            }
            else
            {
                streak =0;
                again = "n";
            }
        }
        while(again.equals("y"));

        out.println("Final score: "+streak);

        if(streak>best)
        {
            out.println("That's a new high score");
            out.print("Your name: ");
            bestName = kb.next();
            best = streak;
        }
        else if(streak==best)
        {
            out.println("That ties the high score. Good job!");
        }
        else
        {
            out.println("You'll have to do better than "+streak+ " to get a high score");
        }

        // Save the name and high score of the winner
        PrintWriter output = new PrintWriter(new FileWriter(saveFile));
        output.println(bestName);
        output.println(best);
        output.close();
    }
}
