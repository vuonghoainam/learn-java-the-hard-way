import java.util.Scanner;
import static java.lang.System.*;



// This one is to create a infinite loop
public class Ex28{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		int secret, guess;

		secret = 1 + (int) (Math.random()*10);

		out.print("I have chosen a number between 1 and 10. Try to guess it\nYour guess: ");
		guess = keyboard.nextInt();

		while (secret != guess){
			out.print("Wrong");
		}
		out.print("right");
	}
}