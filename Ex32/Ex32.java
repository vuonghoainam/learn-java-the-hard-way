// The simple version of Pig game

import java.util.*;
import static java.lang.System.*;

public class Ex32{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		int roll, total;
		total = 0;

		do{
			roll = 1 + (int)(Math.random()*6);
			out.println("\nComputer rolled a "+roll);

			if(roll ==1){
				out.print("\tThat ends its turn");
				total = 0;
			}
			else{
				total += roll;
				out.print("\tComputer has "+total+" points so far this round");

				if(total <20){
					out.print("\tComputer choose to roll again");
				}
			}
		}
		while(roll != 1 && total<20);

		out.print("\nComputer ends the round with "+total+" points");
	}
}