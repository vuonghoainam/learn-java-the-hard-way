// Records from a File

// The import of class from other files
import java.util.Scanner;
import static java.lang.System.*;

class Actor{// Consider a class as a record which contains many fields
    String name, role, birthDate;
}

public class Ex54{
    public static void main(String[] args) throws Exception
    {
        String url = "http://learnjavathehardway.org/txt/s01e01-cast.txt";

        Scanner inFile = new Scanner((new java.net.URL(url)).openStream());

        while(inFile.hasNext()){
            Actor a = getActor(inFile);
            out.print(a.name +" was born on "+ a.birthDate);
            out.println(" and played the role of " + a.role);
        }
        inFile.close();
    }

    public static Actor getActor(Scanner input){
        Actor a = new Actor();
        a.name = input.nextLine();
        a.role = input.nextLine();
        a.birthDate = input.nextLine();

        return a;
    }
}
