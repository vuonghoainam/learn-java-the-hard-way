// Nested For loops

import static java.lang.System.*;

public class Ex46{
    public static void main(String[] args){
        // This is #1 - as "CN"
        for(char c='A';c<='E';c++){
            for(int n=1;n<=3;n++){
                out.println(c+" "+n);
            }
        }
        out.println("\n");

        // this is #2 - as "AB"
        for (int a =1;a<=3;a++){
            for(int b=1;b<=3;b++){
                out.println("("+a+","+b+")");
            }
        }
        out.println("\n");
    }
}
