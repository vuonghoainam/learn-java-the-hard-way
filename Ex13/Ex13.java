import java.util.Scanner;

public class Ex13{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);

		String word;
		boolean yep, nah;

		System.out.println("Type the word \"weasel\" please");
		word = keyboard.next();

		yep = word.equals("weasel");
		nah = ! word.equals("weasel");

		System.out.println("You typed in the requested word?" + yep + " " + nah);
	}
}