import java.util.Scanner; // This one to read info from the keyboard or files or Internet

public class Ex7{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in); // Create and name the method from scanner to read input from keyboard

		int userInput;

		System.out.println("What is the capital of France?");
		keyboard.next();

		System.out.println("What is 6*7?");
		userInput = keyboard.nextInt();

		System.out.println("So, your answer is:"+ userInput);

		System.out.println("What is your favorite number between 0.0 and 1.0?");
		keyboard.nextDouble();

		System.out.println("Is there anything else you would like to tell me?");
		keyboard.next();
	}
}