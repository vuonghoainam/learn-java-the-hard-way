// Final Project - Text adventure game

import java.util.Scanner;
import static java.lang.System.*;

class Room
{
    int roomNumber;
    String roomName, description;
    int numExits;
    String[] exits = new String[10];
    int[] destinations = new int[10];
}

public class Ex58{
    public static void main(String[] args){
        Scanner kb = new Scanner(in);

        // Initialize rooms from file
        Room[] rooms = loadRoomsFromFile("text-adventure-rooms.txt");

        showAllRooms(rooms);

        // Play the game
        int currentRoom = 0;
        String ans;

        while(currentRoom>=0){
            Room cur = rooms[currentRoom];
            out.print(cur.description);
            out.print("> ");
            ans = kb.nextLine();

            // See if what they typed match any of the exit names
            boolean found = false;

            for(int i=0;i<cur.numExits;i++){
                if(cur.exits[i].equals(ans))
                {
                    found = true;

                    // If so, change our next room to that exit's room number
                    currentRoom = cur.destinations[i];
                }
            }
            if(!found){
                out.println("Sorry, I do not understand");
            }
        }
    }

    public static Room[] loadRoomsFromFile(String filename)
    {
        Scanner file = null;
        try
        {
            file = new Scanner(new java.io.File(filename));
        }
        catch(java.io.IOException e)
        {
            err.println("Sorry, I can't read from the file '"+filename+"'.");
            exit(1);
        }

        int numRooms = file.nextInt();
        Room[] rooms = new Room[numRooms];

        // Initialize rooms from file
        int roomNum = 0;

        while(file.hasNext())
        {
            Room r = getRoom(file);

            if(r.roomNumber != roomNum)
            {
                err.println("Reading room # "+r.roomNumber+", but "+roomNum+" was expected");
                exit(2);
            }

            rooms[roomNum] = r;
            roomNum++;
        }
        file.close();

        return rooms;
    }

    // Checked, this function is ok
    public static void showAllRooms(Room[] rooms)
    {
        for(Room r:rooms)
        {
            String exitString="";

            for(int i = 0;i<r.numExits;i++)
            {
                exitString += "\t"+r.exits[i]+" ("+r.destinations[i]+")";
                out.println(r.roomNumber+")" + r.roomName+"\n"+exitString);
            }
        }
    }

    public static Room getRoom(Scanner f)
    {
        // Any rooms left in the file?
        if(!f.hasNextInt())
            return null;

        Room r = new Room();
        String line;

        // read in the room # for error-checking later
        r.roomNumber = f.nextInt();
        f.nextLine();// Skip "\n" after room #
        r.roomName = f.nextLine();

        // Read in the room's description
        r.description = "";

        while(true)
        {
            line = f.nextLine();

            if(line.equals("%%"))
                break;
            r.description += line + "\n";
        }

        // Finally, we'll read in the exits
        int i = 0;

        while(true)
        {
            line = f.nextLine();

            if(line.equals("%%"))
                break;

            String[] parts = line.split(":");

            r.exits[i] = parts[0];
            r.destinations[i] = Integer.parseInt(parts[1]);
            i++;
        }
        r.numExits=i;

        // Should be done; return the Room
        return r;
    }
}
