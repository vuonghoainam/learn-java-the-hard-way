// Thirty days revisited with javadoc
// Javadoc is an automatic doc generating tool that is included with the Java compiler
// We write docment right in code by doing special sort of block comment above functions,classes or variables
import java.util.Scanner;
import static java.lang.System.*;

/**
* Contains functions that make it easier to work with months
**/
public class Ex39{
    public static void main(String[] args){
        Scanner kb = new Scanner(in);

        out.print("Which month?(1-12) ");
        int month = kb.nextInt();

        out.print(monthDays(month)+" days hath "+monthName(month));
    }
    /**
    * Returns the name for the given month number (1-12)
    * @author: Vuong Hoai Nam
    * @param month the month number(1-12)
    * @return       a String containing the English name for the given month or "error" if month out of range
    */
    public static String monthName(int month){
        String monthName;

        if(month==1){
            monthName="Jan";
        }
        else if(month==2){
            monthName="Feb";
        }
        else if(month==3){
            monthName="Mar";
        }
        else if(month==4){
            monthName="Apr";
        }
        else if(month==5){
            monthName="May";
        }
        else if(month==6){
            monthName="Jun";
        }
        else if(month==7){
            monthName="Jul";
        }
        else if(month==8){
            monthName="Aug";
        }
        else if(month==9){
            monthName="Sep";
        }
        else if(month==10){
            monthName="Oct";
        }
        else if(month==11){
            monthName="Nov";
        }
        else if(month==12){
            monthName="Dec";
        }
        else{
            monthName="error";
        }

        return monthName;
    }

    /**
    * Returns the number of days in a given month
    * @author Vuong Hoai Nam
    * @param month the month number(1-12)
    * @return       the number of days in a non- leap year for that month, or 31 if month out of range
    **/
    public static int monthDays(int month){
        int days;

        /**
        Thirty days hath Septer ber, april, Hune and November
        **/

        switch(month){
            case 9:
            case 4:
            case 6:
            case 11: days = 30;
            break;
            case 2: days = 28;
            break;
            default: days=31;
        }
        return days;
    }
}
