public class Ex3{
	public static void main(String[] args){
		System.out.println("Alpha");
		System.out.println();
		System.out.println("Beta");
	}
}

// Must remember the differences between print() and println()

// println() moves to a new line after finishing printing, the other one does not: it displays and then leaves the cursor right at the end of the line so that the following printing statement picks up from that same position on the line