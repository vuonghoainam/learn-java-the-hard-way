import java.util.Scanner;
import static java.lang.System.*;

public class Ex27{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		int secret, guess;

		secret = 1 + (int)(Math.random() * 100);

		out.print("I'm thinking of a number between 1-100. Try to guess "+secret);
		out.print("\n> ");
		guess = keyboard.nextInt();

		while(secret != guess){
			if(guess<secret){
				out.print("Too low");
			}
			if(guess> secret){
				out.print("Too high");
			}
			out.print("\n> ");
			guess = keyboard.nextInt();
		}
		out.print("How can you guess it right???");
	}
}