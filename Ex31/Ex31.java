import java.util.*;
import static java.lang.System.*;

public class Ex31{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		int current, total = 0;

		out.print("Type in a bunch of values and I'll add them up.\nI'll stop when when you type a zero");

		do{
			out.print("\nValue: ");
			current = keyboard.nextInt();
			int newtotal = current + total;
			total = newtotal;
			out.print("The total so far is: "+total);
		}
		while( current != 0);

		out.print("\nThe final total is: "+total);
	}
}