// Saying something not in an Array

import java.util.Scanner;
import static java.lang.System.*;

public class Ex50{
    public static void main(String[] args){
        Scanner kb = new Scanner(in);

        String[] heroes = {
            "Nam","My","Bảo"
        };
        String guess;
        boolean found;

        out.print("Pop Quiz! Name any heores ");
        guess = kb.nextLine();

        found = false;

        for(String hero: heroes){
            if(guess.equals(hero)){
                out.println("That's correct");
                found = true;
            }
        }

        if(found==false){
            out.println("No, "+guess+" is not the hero we want");
        }
    }
}
