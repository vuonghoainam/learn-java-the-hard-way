// Finding things in Array

import java.util.Scanner;
import static java.lang.System.*;

public class Ex49{
    public static void main(String[] args){
        Scanner kb = new Scanner(in);

        int[] orderNumbers = {12345,54321,58653,67346,73462,12463,735346};
        int toFind;

        out.println("There are " +orderNumbers.length+ " orders in the database");

        out.print("\n Orders: ");
        for ( int num : orderNumbers ){
            out.print(num+" ");
        }
        out.println();
        out.println("Which order to find? ");
        toFind = kb.nextInt();

        for(int num:orderNumbers){
            if(num==toFind){
                out.println(num+" found.");
            }
            else{
                out.println("Error");
            }
        }
    }
}
