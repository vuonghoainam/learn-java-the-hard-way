// A deck of plauing cards
import static java.lang.System.*;

class Card{
    int value;
    String suit,name;

    // The method ~ function in class
    public String toString(){
        return name+" of "+suit;
    }
}

public class Ex57{
    public static void main(String[] args) throws Exception
    {
        Card[] deck = buildDeck();
        //displayDeck(deck);

        int chosen = (int)(Math.random()*deck.length);
        Card pickedCard = deck[chosen];

        out.println("You picked a "+pickedCard+" out of the desk. In Backjack your card is worth "+pickedCard.value+" points");
    }

    public static Card[] buildDeck()
    {
        String[] suits = {"clubs","diamonds","hearts","spades"};
        String[] names = {"0","1","2","3","4","5","6","7","8","9","10","Jack","Queen","King","Ace"};

        int i = 0;
        Card[] deck = new Card[52];// Specify the length of the array

        for(String suit:suits){
            for(int v = 2;v<=14;v++){
                Card c = new Card();

                c.suit = suit;
                c.name = names[v];

                if(v == 14)
                    c.value = 11;
                else if(v>10)
                    c.value = 10;
                else
                    c.value = v;

                deck[i] = c;
                i++;
            }
        }
        return deck;
    }

    public static void displayDeck(Card[] deck)
    {
        for(Card eachCard : deck)
            out.println(eachCard.value +"\t" + eachCard);
    }
}
