// Getting data from a file

import java.util.Scanner;
import java.io.File;
import static java.lang.System.*;

public class Ex42{
    public static void main(String[] args) throws Exception
    {
        Scanner fileIn = new Scanner(new File("letter.txt"));

        String name,a,b,c,d;

        out.print("Getting content from a file...");
        name = fileIn.nextLine();
        a = fileIn.nextLine();
        b= fileIn.nextLine();
        c= fileIn.nextLine();
        d= fileIn.nextLine();
        fileIn.close();

        out.println("Done");
        out.println("\n"+name);
        out.println("\n"+a+"\n"+b+"\n"+c+"\n"+d);
    }
}
