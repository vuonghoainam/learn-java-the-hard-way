import java.util.*;
import static java.lang.System.*;

public class Ex34{
  // When main() ends, the program ends as well -> one main() per file
  public static void main(String[] args){
    out.print("Here.");
    erebor();
    out.print("Back first time.");
    erebor();
    out.print("Back second time.");
  }

  public static void erebor(){
    out.print("There.");
  }
}
