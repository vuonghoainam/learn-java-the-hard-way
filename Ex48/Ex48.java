// Doing something with arrays
import static java.lang.System.*;

public class Ex48{
    public static void main(String[] args){
        String[] planets = {"Mercury","Venus","Earth","Mars","Jupiter","Saturn",
                            "Uranus","Neptune"};
        for(String p: planets){// This one to loop through each value in the array ~~ forEach
            out.println(p+"\t"+p.toUpperCase());
        }
    }
}
