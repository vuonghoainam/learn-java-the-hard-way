// Arrays without Foreach Loops

/**
    The foreach loop has some disadvantages
        - Cannot iterate backward
        - Read- only -> cannot change value
        - A Foreach only works with a specified array
        - Best suited with small Arrays
        - cannot itereate throgh a array from list
**/
import static java.lang.System.*;

public class Ex51{
    public static void main(String[] args){
        int[] arr = new int[3];
        int i;

        arr[0]=0;
        arr[1]=0;
        arr[2]=0;

        out.println("Array contains: "+arr[0]+" "+arr[1]+" "+arr[2]);
        arr[0] = 1 + (int)(Math.random()*100);
        arr[1] = 1 + (int)(Math.random()*100);
        arr[2] = 1 + (int)(Math.random()*100);

        i=0;
        arr[i] = 1;
    }
}
