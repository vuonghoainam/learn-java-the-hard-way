// This ex is to random number
import static java.lang.System.*;


public class Ex24{
	public static void main(String[] args){
		int a,b,c;
		double x,y,z;

		x = Math.random();
		y = Math.random();
		z = Math.random();

		out.print("x: "+x+" y: "+y+" z: "+z);

		// these ones we have to be careful about
		a = (int)x;
		b = (int)y;
		c = (int)z;
		out.print("a: "+a+" b: "+b+" c: "+c);
	}
}