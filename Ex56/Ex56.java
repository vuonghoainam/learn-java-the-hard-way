// Array of classes from a File(temperatures revisited)

import java.util.Scanner;
import static java.lang.System.*;

class Temperatures{
    int month, day, year;
    double temperature;
}

public class Ex56{
    public static void main(String[] args) throws Exception
    {
        String url = "http://learnjavathehardway.org/txt/avg-daily-temps-with-dates-atx.txt";

        Scanner inFile = new Scanner((new java.net.URL(url)).openStream());

        Temperatures[] tempDB = new Temperatures[1000];// 1st type of declaring

        int numRecords, i = 0;

        // This while loop equals to for loop
        while(inFile.hasNextInt() && i<tempDB.length){
            Temperatures e = new Temperatures();// 2nd type of declaring
            e.month = inFile.nextInt();
            e.day = inFile.nextInt();
            e.year = inFile.nextInt();
            e.temperature = inFile.nextDouble();

            if(e.temperature ==-99)// If there is no data available
                continue;
            tempDB[i] = e;
            i++;
        }
        inFile.close();
        numRecords = i;

        out.println(numRecords+" daily temperatures loaded");

        double total = 0,avg;
        int count = 0;

        for(i=0;i<numRecords;i++){
            if(tempDB[i].month ==11){
                total += tempDB[i].temperature;
                count++;
            }
        }

        avg = total/count;
        avg = roundToOneDecimal(avg);

        out.println("Average daily temp over "+count+" days of November: "+avg);
    }

    public static double roundToOneDecimal(double d){
        return Math.round(d*10)/10.0;
    }
}
