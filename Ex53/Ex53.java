// Mailing addresses (records)

import static java.lang.System.*;

// The use of class in Java
class Address{
    String street,city,state;
    int zip;
}

public class Ex53{
    public static void main(String[] args){
        Address uno,dos,tres;

        uno = new Address();
        uno.street = "Nguyen Chi Thanh";
        uno.city = "Hanoi";
        uno.state = "Dong Da";
        uno.zip = 10000;

        dos = new Address();
        dos.street = "Nam";
        dos.city = "My";
        dos.state = "Minh";
        dos.zip = 2000;

        out.println(uno.street);
        out.println("\n"+dos.zip);
    }
}
