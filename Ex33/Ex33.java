// The dice game called Pig- the complex version

import java.util.*;
import static java.lang.System.*;

public class Ex33{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		int roll, total1, total2, turnTotal;
		String choice = "";

		total1 = 0;
		total2 = 0;

		do{
			turnTotal = 0;
			out.print("\nYou rolled a "+total1+" roll");

			do{
				roll = 1+(int)(Math.random()*6);
				out.print("\nYou rolled a "+roll+" roll");
				if(roll ==1){
				out.print("\nThat ends your turn");
				turnTotal = 0;
				}
				else{
					turnTotal += roll;
					out.print("\nYou have "+turnTotal+" points so far this round");
					out.print("\nWould you like to \"roll\" again or \"hold\"?");
					choice = keyboard.next();
				}
		}
		while(roll != 1 && choice.equals("roll"));

		total1 += turnTotal;
		out.print("\nYou end the round with "+total1+" points");

		if (total1<100){
			turnTotal = 0;
			out.print("\nComputer has "+ total2+" points");

			do{
				roll = 1+(int)(Math.random()*6);
				out.print("\nComputer rolled a "+roll);

				if(roll ==1){
					out.print("\nThat ends its turn");
					turnTotal = 0;
				}
				else{
					turnTotal += roll;
					out.print("\nComputer has "+turnTotal+" points so far this round");

					if(turnTotal<20){
						out.print("\nComputer choose to roll again");
					}
				}
			}while(roll != 1 && turnTotal<20);
			}
			
			total2 += turnTotal;
			out.print("\nComputer ends the round with "+total2+" points");
		}
		while(total1<100 && total2<100);

		if(total1>total2){
			out.print("Human wins");
		}
		else{
			out.print("Computer wins");
		}
	}
}