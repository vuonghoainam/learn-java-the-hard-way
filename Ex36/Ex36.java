// Displaying dice with functions???
// Same as Ex 35
import static java.lang.System.*;

public class Ex36{
    public static void main(String[] args){
        int roll1, roll2, roll3, roll4, roll5;
        boolean allTheSame;

        do
        {
            roll1 = 1 +(int)(Math.random()*6);
            roll2 = 1 +(int)(Math.random()*6);
            roll3 = 1 +(int)(Math.random()*6);
            roll4 = 1 +(int)(Math.random()*6);
            roll5 = 1 +(int)(Math.random()*6);

            out.print("\nYou rolled: "+roll1+" "+roll2+" "+roll3+" "+roll4+" "+roll5);

            showDice(roll1);
            showDice(roll2);
            showDice(roll3);
            showDice(roll4);
            showDice(roll5);
            allTheSame = (roll1==roll2 && roll2==roll3 && roll3==roll4 && roll4==roll5);
        }
        while(! allTheSame);
        out.println("The Yacht!");
    }
    // The params of function must be called with type
    public static void showDice(int roll){
        out.println("\n+---+");

        if(roll==1){
            out.println("|   |");
            out.println("| o |");
            out.println("|   |");
        }
        else if(roll==2){
            out.println("|o  |");
            out.println("|   |");
            out.println("|  o|");
        }
        else if(roll==3){
            out.println("|o  |");
            out.println("| o |");
            out.println("|  o|");
        }
        else if(roll==4){
            out.println("|o o|");
            out.println("|   |");
            out.println("|o o|");
        }
        else if(roll==5){
            out.println("|o o|");
            out.println("| o |");
            out.println("|o o|");
        }
        else if(roll==6){
            out.println("|o o|");
            out.println("|o o|");
            out.println("|o o|");
        }
        out.println("+---+");
    }
}
