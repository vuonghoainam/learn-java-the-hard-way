// Returning a value from a function
// Params are the only way to send values into a function
// Only way to get a value out of a function: Using return value

import static java.lang.System.*;

public class Ex37{
    public static void main(String[] args){
        double a;
        // a here means a = returned A
        a = triangleArea(3,3,3);
        out.println("A triangle with slides 3,3,3 has an area of "+a);

        a = triangleArea(3,4,5);
        out.println("A triangle with slides 3,4,5 has an area of "+a);

        out.println("a triangle with slides 10,9,11 has an area of "+triangleArea(10,9,11));
    }

    // This funcition contains no void between static and triangleArea, void means function returns nothing
    // It shows that, the function will return values with type of double
    public static double triangleArea(int a, int b, int c){
        // The code in this function computes the area of a triangle whose slides have length of a,b,c
        double s,A;

        s = (a+b+c)/2;
        A = Math.sqrt(s*(s-a)*(s-b)*(s-c));

        return A;
    }
}
