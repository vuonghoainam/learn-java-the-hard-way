// Counting with a For Loop

import java.util.Scanner;
import static java.lang.System.*;

public class Ex44{
    public static void main(String[] args){
        Scanner kb = new Scanner(in);

        int n;
        String message;

        out.println("Type in a message, and I'll display it five times");
        out.println("\nMessage: ");
        message = kb.nextLine();

        for(n=1;n<=5;n++){
            out.println(n+" "+message);
        }

        out.println("\nNow I'll display ten times and count by 5s");

        for(n=5;n<=50;n++){
            out.println(n+" "+message);
        }

        out.println("\nFinally three times couting backwards");

        for(n=3;n>0;n-=1){
            out.println(n+" "+message);
        }
    }
}
