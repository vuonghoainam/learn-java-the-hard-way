// Importing standard libraries

import java.util.Scanner;
import java.security.MessageDigest;
import javax.xml.bind.DatatypeConverter;// In future, use bcrypt lib to convert pwd
import static java.lang.System.*;

public class Ex40{
    // Notice the "throws Exception"
    public static void main(String[] args) throws Exception
    {
        Scanner keyboard = new Scanner(in);

        String pw, hash;

        // Initialize the MessageDigest type of string
        MessageDigest digest  = MessageDigest.getInstance("SHA-256");

        out.print("Password: >");
        pw = keyboard.nextLine();// nextLine used to let user type in more than 1 string

        digest.update(pw.getBytes("UTF-8"));
        hash = DatatypeConverter.printHexBinary(digest.digest());

        out.println(hash);
    }
}
