import java.util.Scanner;
import static java.lang.System.*;

public class Ex26{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		int pin, entry;

		pin = 12345;

		out.print("WELCOME TO THE BANK OF JAVA");
		out.print("\nENTER YOUR PIN ");
		entry = keyboard.nextInt();

		while (entry != pin){
			out.print("\nINCORECT PIN. TRY AGAIN");
			out.print("\nENTER YOUR PIN");
			entry = keyboard.nextInt();
		}

		out.print("\nPIN ACCEPTED. YOU NOW HAVE ACCESS TO YOUR ACCOUNT");
	}
}