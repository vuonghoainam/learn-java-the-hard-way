import java.util.Scanner;
import static java.lang.System.*;

public class Ex22{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		int month, days;
		String monthName;

		out.print("Which month? (1-12) ");
		month = keyboard.nextInt();

		switch(month){
			case 1: monthName = "Jan";
			break;
			case 2: monthName = "Feb";
			break;
			default: monthName = "error";
		};
		out.println("You typed in the month "+ monthName);
	}
}