import static java.lang.System.*;

// Call many functions
public class Ex35{
  public static void main(String[] args){
    printTopHalf();

    print1Colon();
    print1Oh();
  }

  public static void printTopHalf(){
      out.print("Hello World!");
  }
  public static void print1Colon(){
      out.print(":");
  }
  public static void print1Oh(){
      out.print("hhhhhhhhhh");
  }
}
