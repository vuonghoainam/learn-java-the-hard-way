import java.util.*;
import static java.lang.System.*;

public class Ex30{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		String coin, again;
		int flip, streak = 0;

		do{
			flip = 1 + (int)(Math.random()*2);

			if (flip ==1){
				coin = "HEADS";
			}
			else{
				coin = "Tails";
			}

			out.print("You flip a coin and it is "+coin);

			if (flip ==1){
				streak++;
				out.println("\tThat's "+streak+" in a row");
				out.print("\tWould you like to flip again (y/n)? ");
				again = keyboard.next();
			}
			else{
				streak = 0;
				again = "n";
			}
		}
		while(again.equals("y"));
		out.print("\nFinal score: "+streak);
	}
}