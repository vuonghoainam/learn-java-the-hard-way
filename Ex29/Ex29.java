import java.util.Scanner;
import static java.lang.System.*;

public class Ex29{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		double x,y;

		out.print("Give me the number and I will find the squre root of it\nAnd no negative please ");
		x = keyboard.nextInt();

		while(x<0){
			out.print("Sorry, I won't take the square root of a negative. Please type in new input\n");
			x = keyboard.nextInt();
		}
		y = Math.sqrt(x);
		out.print("The square root of "+x+" is "+y);
	}
}