// Caesar Cipher(Looping through a String)

import java.util.Scanner;
import static java.lang.System.*;

public class Ex45{
    /**
    * Returns the character shifted by the given number of letters
    **/
    public static char shiftLetter(char c, int n){
        int ch =c;
        if(! Character.isLetter(c)){// Function .isLetter
            return c;
        }
        ch = ch+n;

        if(Character.isUpperCase(c) && ch>'z' || Character.isLowerCase(c) && ch>'z'){
            ch-=26;
        }
        if(Character.isUpperCase(c) && ch<'a' || Character.isLowerCase(c) && ch<'a'){
            ch+=26;
        }

        return (char)ch;// char type, holds 1 character at a time
    }

    public static void main(String[] args){
        Scanner kb = new Scanner(in);
        String plaintext, cipher="";
        int shift;

        out.print("Message: ");
        plaintext = kb.nextLine();
        out.print("\nShift(0-26) ");
        shift = kb.nextInt();

        for(int i=0;i<plaintext.length();i++){
            cipher += shiftLetter(plaintext.charAt(i),shift);// Function .charAt()
        }
        out.println(cipher);
    }
}
