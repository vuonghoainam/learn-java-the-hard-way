// this one is to compare strings in alphabetical order

import java.util.Scanner;
import static java.lang.System.*;

public class Ex23{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(in);

		String name;
		out.print("Give me the name of a programming language? ");
		name = keyboard.next();

		if (name.compareTo("java")<0)
			out.print(name + " comes before java");
	}
}