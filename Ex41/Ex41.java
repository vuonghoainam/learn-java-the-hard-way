import java.io.FileWriter;
import java.io.PrintWriter;

public class Ex41{
    // the "throws Exception" means: The function may be wrong, if wrong, it keeps continue
    // This one equals try... catch statement
    public static void main(String[] args) throws Exception
    {
        PrintWriter fileout = new PrintWriter(new FileWriter("letter.txt"));

        fileout.println("I am Nam");
        fileout.println("It is nice to meet you");
        fileout.close();// Add this line so that the created file will not be empty
    }
}
