// Lowest temperature

// Find the lowest temperature in the find then convert it to Farhrenheit degree
import java.net.URL;// This one is to read from URL, with Internet available
import java.util.Scanner;
import static java.lang.System.*;

public class Ex52{
    public static void main(String[] args) throws Exception
    {
        double[] temps = arrayFromUrl("http://learnjavathehardway.org/txt/avg-daily-temps-atx.txt");

        out.println(temps.length+" temperatures in database");

        double lowest = 9999.99;

        for (int i=0;i<temps.length;i++){
            if(temps[i]<lowest){
                lowest = temps[i];
            }
        }
        out.println("The lowest average daily temperature was "+lowest+"*F ("+fToC(lowest)+"*C)");
    }

    public static double[] arrayFromUrl(String url) throws Exception
    {
        Scanner fin = new Scanner(new URL(url).openStream());
        int count = fin.nextInt();

        double[] dubs = new double[count];

        for(int i=0;i<dubs.length;i++){
            dubs[i] = fin.nextDouble();
        }
        fin.close();

        return dubs;
    }

    public static double fToC(double f){
        return (f-32)*5/9;
    }
}
