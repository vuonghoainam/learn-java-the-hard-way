import java.util.Scanner;

public class Ex15{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);

		int age;

		System.out.print("How old are you ");
		age = keyboard.nextInt();

		if (age <13){
			System.out.print("You are too young to create a Facebook account");
		}
	}
}